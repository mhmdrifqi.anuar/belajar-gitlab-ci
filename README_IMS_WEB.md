# IMS Web Services

## Pre Requisite
- Node v14.21.3

## Build Step
#### Langkah konfigurasi file ENV pada IMS Web Service
buat file `.env` pada root folder project
- sesuaikan config base_url untuk dimapping kan pada URL IMS Backend Service
```sh
PORT=3000
CHOKIDAR_USEPOLLING=true
REACT_APP_API_URL=https://URL_IMS_WEB_SERVICE/apis
REACT_APP_WEBSOCKET_URL=https://URL_IMS_WEB_SERVICE/apis/web-socket
CI=false
```
> Note : untuk IMS Backend Service menggunakan subdomain yang sama dengan Frontend tetapi ada tambahan prefix /apis untuk mengarahkan ke IMS backend service. config ini dilakukan di nginx dengan konsep nginx rewrite + proxy_pass yang mengarah ke IMS Backend Service

#### Langkah Build package
- pastikan terminal / cmd berada pada root directory project ims-web-service
- jalankan perintah berikut untuk install library pada code
```sh
npm install
```
- jalankan perintah berikut untuk melakukan compile pada code
```sh
npm run build
```
- setelah step compile selesai, hasil compile akan berada pada folder `build` dan isi folder berikut yang akan diupload ke server.

#### Langkah Deploy Service
> Note : Langkah ini dilakukan pada server deployment

##### Pre Requisite
- File SSL
- Nginx 1.18 or Latest
##### Step Deployment IMS Web Service & Konfigurasi Nginx
- upload isi folder `build` beserta subfolder didalam nya ke folder `/usr/share/nginx/html`
- buat config nginx dengan cara copy value dibawah ini ke folder `/etc/nginx/conf.d/ims-app.conf` & jangan lupa disesuaikan 
```sh
server {
        listen 80;
        server_name SUBDOMAIN.pegadaian.co.id;
        return 301 https://$server_name$request_uri;
}

server {
        listen 443 ssl http2;
        server_name SUBDOMAIN.pegadaian.co.id;
        ssl_certificate /etc/ssl/fullchain.pem;
        ssl_certificate_key /etc/ssl/privkey.pem;
        ssl_ciphers ALL:!ADH:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EX;
        
        ## Config Mapping IMS Web Service
        location / {
            index index.html;
            root /usr/share/nginx/html;
            try_files $uri $uri/ /index.html;
        }
        
        ## Config Mapping IMS Backend Service
        location /apis/ {
            rewrite /apis/(.*) /$1  break;
            proxy_pass         http://localhost:9091;
            proxy_redirect     off;
            proxy_set_header   Host $host;
        }
}
```
> Note : Sesuaikan value ssl_certificate , ssl_certificate_key, dan SUBDOMAIN server
- setelah config nginx selesai lakukan validasi config dengan menjalankan perintah
```sh
sudo nginx -t
```
- jika tidak ada issue anda bisa menerapkan config nginx dengan melakukan reload service nginx
```sh
sudo service nginx reload
```