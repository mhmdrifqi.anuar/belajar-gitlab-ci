# IMS Backend Services

## Pre Requisite
- Java 8 / OpenJDK 8
- Apache Maven 3.5.0

## Build Step
#### Langkah konfigurasi file ENV pada IMS Backend Service
sesuaikan file `application.properties` pada folder `/src/main/resources`
- sesuaikan config database
```sh
## Database Config
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://IP_HOST:5432/imsdb_v2
spring.datasource.username=postgres
spring.datasource.password=password
```
- sesuaikan config PASSION
```sh
## PASSION Credentials
passion.token-username=9989
passion.token-password=inven123
passion.auth-username=aplikasiinven
passion.auth-password=aplikasiinven123
passion.channel-id=6017
passion.client-id=9997
```
- sesuaikan config Minio
```sh
## Setting Minio
minio.url=https://minio_uri:9000/
minio.username=miniousr
minio.password=miniopass
minio.bucketName=pegadaian-bucket
```
- sesuaikan config Firebase + IMS Proxy
```sh
## Setting firebase
app.firebase-configuration-file=./serviceAccountKey.json
app.firebase-dmz=https://url_ims_proxy_service
app.enable-firebase=true
app.use-dmz-firebase-publisher=false
```

#### Langkah Build package
- pastikan terminal / cmd berada pada root directory project ims-backend-service
- jalankan perintah berikut untuk install library pada code
```sh
mvn install
```
- jalankan perintah berikut untuk melakukan compile pada code
```sh
mvn package
```
- setelah step compile selesai, hasil compile akan berada pada folder `target` dengan nama file `imsapi-v0.0.1-exec.jar` file berikut yang akan dideploy ke server

#### Langkah Deploy Service
Note : Langkah ini dilakukan pada server deployment
- buat folder untuk menyimpan service dengan menjalankan perintah 
```sh
mkdir -p /opt/ims-backend
```
- buat folder untuk menyimpan log service dengan menjalankan perintah 
```sh
mkdir -p /var/log/ims-log
```
- upload file `imsapi-v0.0.1-exec.jar` dan simpan pada folder `/opt/ims-backend`
- beri akses executable pada file `imsapi-v0.0.1-exec.jar` dengan menjalankan perintah 
```sh
chmod +x /opt/ims-backend/imsapi-v0.0.1-exec.jar
```
- langkah selanjutnya kita akan membuat service tersebut berjalan as service pada linux, dengan cara copy semua value dibawah ini dan simpan menjadi file pada folder `/etc/systemd/system/ims-backend.service`
> Note: sesuaikan value User dengan username pada server anda.
```sh
[Unit]
Description=IMS Backend Service

[Service]
WorkingDirectory=/opt/ims-backend/
ExecStart=java -jar imsapi-v0.0.1-exec.jar
User=user
Type=simple
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
```
- lalu set permission pada folder `/opt/ims-backend` dengan menjalankan perintah
> Note: sesuaikan $USER dengan username pada server anda.

```sh
sudo chown -R $USER:$USER /opt/ims-backend
```
- lalu jalankan perintah untuk reload config systemd
```sh
sudo systemctl daemon-reload
```
- lalu jalankan perintah berikut untuk running service ims-backend
```sh
sudo systemctl start ims-backend.service
```
- dan berikut perintah lainnya untuk manage ims backend service
```sh
sudo systemctl start ims-backend.service = untuk start ims-backend service
sudo systemctl restart ims-backend.service = untuk restart ims-backend service
sudo systemctl stop ims-backend.service = untuk stop ims-backend service
sudo systemctl status ims-backend.service = untuk check status ims-backend service
```

### Troubleshoot Service
- step berikut dilakukan untuk melihat log + follow log pada IMS Backend Service
```sh
tail -f /var/log/ims-log/PEGADAIAN_IMS.DEV_SYS.log
```
- atau bisa juga menggunakan command dibawah ini untuk melihat log berdasarkan jumlah garis terbaru
```sh
tail -n 1000 /var/log/ims-log/PEGADAIAN_IMS.DEV_SYS.log
```
- penjelasan : -n adalah jumlah line pada logs terbaru yang akan diprint pada layar